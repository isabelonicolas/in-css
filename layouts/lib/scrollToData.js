var BForm = (function() {
  "use strict";

  var BForm = function(form = null) {
    var SELF = this;

    SELF.form = form;
    SELF.formData = {};

    if (SELF.form) {
      SELF.initializeEvents();
    }
  };

  BForm.prototype.initializeEvents = function() {
    var SELF = this;

    // Add Events
    SELF.form.addEventListener("submit", function(evt) {
      SELF.handleSubmit(evt, SELF);
    });
  };

  // Actions
  BForm.prototype.handleSubmit = function(evt, SELF) {
    evt.preventDefault();

    axios
      .get("https://yesno.wtf/api")
      .then(function(res) {
        console.log(res.data);
      })
      .catch(function(err) {
        console.log(err);
      });
  };

  return BForm;
})();
