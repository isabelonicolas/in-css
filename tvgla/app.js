'use strict'

var Project = function() {
  this.data = []

  this.init()
}

Project.prototype = {
  init: function() {
    var $this = this

    document.querySelector('#sort-post').addEventListener('click', this.handleSort(e))

    this.loadData(function(res){
      var render = ''
      if (res) {
        $this.data.forEach(function(post, i) {
          render += `<div>${post.title}</div>`
        })
        document.querySelector('.js-recent-posts').innerHTML = render
      } else {
        document.querySelector('.js-recent-posts').innerHTML = 'Failed to load.'
      }
    })
  },
  loadData: function(callback) {
    var $this = this

    $.ajax({
      url: 'https://jsonplaceholder.typicode.com/posts',
      method: 'GET',
      success: function(res) {
        $this.data = res
        return callback(true)
      },
      error: function(err) {
        return callback(false)
      }
    })
  }
  // chars = _.orderBy(chars, ['name'],['asc']); sprt
}

var project_1 = new Project();